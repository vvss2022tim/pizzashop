import org.junit.jupiter.api.Test;
import pizzashop.model.PaymentType;
import pizzashop.repository.MenuRepository;
import pizzashop.repository.PaymentRepository;
import pizzashop.service.AppService;

import static org.junit.jupiter.api.Assertions.*;

class AddPayment_Test_Valid_1_BVA {

    private int tableNumber;
    private double amount;
    private PaymentType paymentType;
    private MenuRepository menuRepo;
    private PaymentRepository payRepo;
    private AppService appService;

    @org.junit.jupiter.api.BeforeEach
    void setUp() {
        menuRepo = new MenuRepository();
        payRepo = new PaymentRepository();
        appService = new AppService(menuRepo,payRepo);
        tableNumber=8;
        amount=1.0;
        paymentType=PaymentType.Card;
    }

    @org.junit.jupiter.api.Test
    void addPayment() {
        assertAll(() -> appService.addPayment(tableNumber,paymentType,amount));
    }
}
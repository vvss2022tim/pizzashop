package integrare;

import org.junit.jupiter.api.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;
import pizzashop.repository.MenuRepository;
import pizzashop.repository.PaymentRepository;
import pizzashop.service.AppService;


import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class IntegrareSR {

    private PaymentRepository repo;
    private AppService service;
    private Payment payment1;
    private Payment payment2;

    @BeforeEach
    void setUp() {
        repo=new PaymentRepository();
        service = new AppService(new MenuRepository(),repo);

        payment1=mock(Payment.class);
        when(payment1.getAmount()).thenReturn(15.0d);
        when(payment1.getType()).thenReturn(PaymentType.Card);
        when(payment1.getTableNumber()).thenReturn(8);

        payment2=mock(Payment.class);
        when(payment2.getAmount()).thenReturn(15.0d);
        when(payment2.getType()).thenReturn(PaymentType.Card);
        when(payment2.getTableNumber()).thenReturn(10);
    }

    @Test
    public void test_add_paymentValid() throws Exception{
        assertEquals(service.getPayments().size(),0);
        try {
            service.addPayment(payment1.getTableNumber(), payment1.getType(),payment1.getAmount());
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        assertEquals(service.getPayments().size(),1);
    }

    @Test
    public void test_add_paymentInvalid() throws Exception{
        assertEquals(service.getPayments().size(),0);
        try {
            service.addPayment(payment1.getTableNumber(), payment1.getType(),payment1.getAmount());
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        assertEquals(service.getPayments().size(),1);
    }

    @Test
    public void test_size_Card_increse() throws Exception{
        assertEquals(service.getPayments().size(),repo.getAll().size());
        repo.add(payment1);
        assertEquals(service.getPayments().size(),repo.getAll().size());
    }
}
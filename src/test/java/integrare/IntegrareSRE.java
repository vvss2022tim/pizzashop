package integrare;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;
import pizzashop.repository.MenuRepository;
import pizzashop.repository.PaymentRepository;
import pizzashop.service.AppService;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class IntegrareSRE {

    private PaymentRepository repo;
    private AppService service;
    private Payment payment1;
    private Payment payment2;

    @BeforeEach
    void setUp() {
        repo = new PaymentRepository();
        service = new AppService(new MenuRepository(),repo);

        payment1 = new Payment(8, PaymentType.Card, 15.0d);
        payment2 = new Payment(10, PaymentType.Card, 15.0d);
    }

    @Test
    public void test_add_paymentValid() throws Exception{
        assertEquals(service.getPayments().size(),0);
        try {
            service.addPayment(payment1.getTableNumber(), payment1.getType(),payment1.getAmount());
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        assertEquals(service.getPayments().size(),1);
    }

    @Test
    public void test_add_paymentInvalid() throws Exception{
        assertEquals(service.getPayments().size(),0);

        Exception thrown = assertThrows(Exception.class, () -> {
            service.addPayment(payment2.getTableNumber(), payment2.getType(),payment2.getAmount());
        });
        assertEquals("Table number is incorrect!", thrown.getMessage());

        assertEquals(service.getPayments().size(),0);
    }

    @Test
    public void test_add_paymentInRepo() throws Exception{
        assertEquals(service.getPayments().size(),0);
        try {
            service.addPayment(payment1.getTableNumber(), payment1.getType(),payment1.getAmount());
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        assertEquals(service.getPayments().size(),1);
    }

    @Test
    public void test_size_Card() throws Exception{
        assertEquals(service.getPayments().size(),repo.getAll().size());
        repo.add(payment1);
        assertEquals(service.getPayments().size(),repo.getAll().size());
    }
}
package serviceMock;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;
import pizzashop.repository.MenuRepository;
import pizzashop.repository.PaymentRepository;
import pizzashop.service.AppService;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

class AppServiceMockTest {

    private PaymentRepository paymentRepository;
    private MenuRepository menuRepository;

    private AppService service;

    @BeforeEach
    void setUp() {
        paymentRepository = mock(PaymentRepository.class);
        menuRepository = mock(MenuRepository.class);

        service = new AppService(menuRepository, paymentRepository);
    }

    @Test
    void test_getPayments() {
        Payment payment = new Payment(1, PaymentType.Card, 12.5);
        Mockito.when(paymentRepository.getAll()).thenReturn(Arrays.asList(payment));

        assertTrue(service.getPayments().equals(Arrays.asList(payment)));
    }

    @Test
    void test_getTotalAmount() {
        Payment payment1 = new Payment(1, PaymentType.Card, 12.5);
        Payment payment2 = new Payment(1, PaymentType.Card, 12.5);
        Mockito.when(paymentRepository.getAll()).thenReturn(Arrays.asList(payment1,payment2));

        assertEquals(service.getTotalAmount(PaymentType.Card),25.0);
    }

}
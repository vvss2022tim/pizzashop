import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pizzashop.model.PaymentType;
import pizzashop.repository.MenuRepository;
import pizzashop.repository.PaymentRepository;
import pizzashop.service.AppService;

import static org.junit.jupiter.api.Assertions.*;

class AddPayment_Test_Invalid_Low_BVA {

    private int tableNumber;
    private double amount;
    private PaymentType paymentType;
    private MenuRepository menuRepo;
    private PaymentRepository payRepo;
    private AppService appService;

    @org.junit.jupiter.api.BeforeEach
    void setUp() {
        menuRepo = new MenuRepository();
        payRepo = new PaymentRepository();
        appService = new AppService(menuRepo,payRepo);
        tableNumber=8;
        amount=0.0;
        paymentType=PaymentType.Card;
    }

    @org.junit.jupiter.api.Test
    void addPayment() {
        Exception thrown = assertThrows(Exception.class, () -> {
            appService.addPayment(tableNumber,paymentType,amount);
        });
        assertEquals(" Amount is incorrect!", thrown.getMessage());
    }
}
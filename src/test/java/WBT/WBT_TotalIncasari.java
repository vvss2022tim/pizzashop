package WBT;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import pizzashop.model.PaymentType;
import pizzashop.repository.MenuRepository;
import pizzashop.repository.PaymentRepository;
import pizzashop.service.AppService;

import static org.junit.jupiter.api.Assertions.*;

class WBT_TotalIncasari {
    private int tableNumber;
    private double amount;
    private PaymentType paymentType;
    private MenuRepository menuRepo;
    private PaymentRepository payRepo;
    private AppService appService;

    @BeforeEach
    void setUp() {
        menuRepo = new MenuRepository();
        payRepo = new PaymentRepository();
        appService = new AppService(menuRepo,payRepo);
        tableNumber=8;
        amount=Double.MAX_VALUE;

    }

    @AfterEach
    void tearDown() {
    }

    @org.junit.jupiter.api.Test
    void totalIncasariPaymentNull() {
        paymentType=null;
        assertEquals(appService.getTotalAmount(paymentType), -1);
    }

    @org.junit.jupiter.api.Test
    void totalIncasariPaymentCardNoEntries() {
        paymentType=PaymentType.Card;
        assertEquals(appService.getTotalAmount(paymentType), 0);
    }

    @org.junit.jupiter.api.Test
    void totalIncasariPaymentCardOneEntryWithCash() {
        paymentType=PaymentType.Card;
        try {
            appService.addPayment(3,PaymentType.Cash,10.0);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        assertEquals(appService.getTotalAmount(paymentType), 0);
    }

    @org.junit.jupiter.api.Test
    void totalIncasariPaymentCardOneEntryWithCard() {
        paymentType=PaymentType.Card;
        try {
            appService.addPayment(3,PaymentType.Card,10.0);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        assertEquals(appService.getTotalAmount(paymentType), 10.0);
    }

    @org.junit.jupiter.api.Test
    void totalIncasariPaymentCardFiveEntryWithCard() {
        paymentType=PaymentType.Card;
        try {
            appService.addPayment(3,PaymentType.Card,10.0);
            appService.addPayment(3,PaymentType.Card,25.0);
            appService.addPayment(3,PaymentType.Card,17.0);
            appService.addPayment(3,PaymentType.Card,13.0);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        assertEquals(appService.getTotalAmount(paymentType), 65.0);
    }



}
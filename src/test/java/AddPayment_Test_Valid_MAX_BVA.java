import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pizzashop.model.PaymentType;
import pizzashop.repository.MenuRepository;
import pizzashop.repository.PaymentRepository;
import pizzashop.service.AppService;

import static org.junit.jupiter.api.Assertions.*;

class AddPayment_Test_Valid_MAX_BVA {
    private int tableNumber;
    private double amount;
    private PaymentType paymentType;
    private MenuRepository menuRepo;
    private PaymentRepository payRepo;
    private AppService appService;

    @org.junit.jupiter.api.BeforeEach
    void setUp() {
        menuRepo = new MenuRepository();
        payRepo = new PaymentRepository();
        appService = new AppService(menuRepo,payRepo);
        tableNumber=8;
        amount=Double.MAX_VALUE;
        amount=amount-Math.pow(10,292);
        paymentType=PaymentType.Card;
    }

    @org.junit.jupiter.api.Test
    void addPayment() {
        System.out.println(amount);
        System.out.println(Double.MAX_VALUE);
        assertAll(() -> appService.addPayment(tableNumber,paymentType,amount));
    }
}
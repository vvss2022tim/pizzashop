package pizzashop.service;

import pizzashop.model.MenuDataModel;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;
import pizzashop.repository.MenuRepository;
import pizzashop.repository.PaymentRepository;
import pizzashop.validation.PaymentAddValidation;

import java.util.List;

public class AppService {

    private MenuRepository menuRepo;
    private PaymentRepository payRepo;

    public AppService(MenuRepository menuRepo, PaymentRepository payRepo) {
        this.menuRepo = menuRepo;
        this.payRepo = payRepo;
    }

    public List<MenuDataModel> getMenuData() {
        return menuRepo.getMenu();
    }

    public List<Payment> getPayments() {
        return payRepo.getAll();
    }

    public void addPayment(int table, PaymentType type, double amount) throws Exception {
        Payment payment = new Payment(table, type, amount);
        PaymentAddValidation validation = new PaymentAddValidation();
        validation.Validation(payment);
        payRepo.add(payment);
    }

    public double getTotalAmount(PaymentType type) {

        if(type!=PaymentType.Card&& type!=PaymentType.Cash)
            return -1;
        double total = 0.0D;
        List<Payment> l = getPayments();
        if ((l == null) || (l.size() == 0)) {
            return total;
        }
        for (Payment p : l) {
            if (p.getType() == type) {
                total += p.getAmount();
            }
        }
        return total;
    }

}
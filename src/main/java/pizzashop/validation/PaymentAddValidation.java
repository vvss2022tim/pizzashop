package pizzashop.validation;

import pizzashop.model.Payment;
import pizzashop.model.PaymentType;

public class PaymentAddValidation extends Exception{

    public void Validation(Payment payment) throws Exception {
        String err = "";
        if(payment.getTableNumber()>8 || payment.getTableNumber()<1)
            err=err + "Table number is incorrect!";
        if(payment.getAmount()<=0.0 || payment.getAmount()>=Double.MAX_VALUE)
            err = err + " Amount is incorrect!";
        if(payment.getType()!=PaymentType.Card && payment.getType()!=PaymentType.Cash)
            err = err + " Type is incorrect!";
        if(!err.equals(""))
            throw new Exception(err);
    }
}
